package com.qatest.oop;

public final class Tomato extends AbsProduct {
    public double weight;
    @Override
    public double getWeight() {
        return weight;
    }
    public Tomato(double weight, double price, int ccal){
        this.weight = weight;
        setPrice(price);
        this.ccal = ccal;
        this.productType = Type.VEGETABLE;
    }

    public Tomato(){
        this.weight = 1.2;
        setPrice(56.25);
        this.ccal = 125;
    }
}
