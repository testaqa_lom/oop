package com.qatest.oop;

public class Steak extends AbsProduct{
    double weight;
    public enum Doneness{
        RARE,MEDIUM,WELL
    }
    private Doneness doneness;

    @Override
    public double getWeight() {
        switch (doneness){
            case RARE: return weight * 0.9;
            case MEDIUM: return weight * 0.8;
            case WELL: return weight * 0.7;
            default: return weight;
        }
    }

    public void setPrice(double price, int days){
        if (days>7){
            setPrice(price * 0.5);
        }
        else setPrice(price);
    }

    public Steak(double weight, Doneness doneness){
        this.weight = weight;
        this.doneness = doneness;
    }
    public Steak(){
        this.weight=0.3;
        this.doneness = Doneness.MEDIUM;
    }
}
