package com.qatest.oop;

public class ChickenSteak extends Steak {
    String info;
    public ChickenSteak(String info, double weight, Doneness doneness){
        super(weight, doneness);
        this.info = info;
    }

}
