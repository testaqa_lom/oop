package com.qatest.oop;

public interface Product {

    double getWeight();
}
