package com.qatest.oop;

public abstract class AbsProduct implements Product {
   protected Type productType;
   private Double price;
   int ccal;

    public Double getPrice() {
        return price;
    }

    public int getCcal() {
        return ccal;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

}
