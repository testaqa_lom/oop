package com.qatest.oop;

public class Main {
    public static void main(String[] args) {
        PorkSteak porkSteak = new PorkSteak();
        porkSteak.setPrice(130.0,3);
        System.out.println("Pork steak price is: "+porkSteak.getPrice());
        porkSteak.setPrice(130.0,8);
        System.out.println("Pork steak price is: "+porkSteak.getPrice());
        ChickenSteak chickenSteak = new ChickenSteak("very tasty! You'll eat your fingers!",0.35, Steak.Doneness.WELL);
        chickenSteak.setPrice(95.0);
        System.out.println(chickenSteak.info);
        Tomato tomato = new Tomato();
        Tomato tomato1 = new Tomato(0.2,10.95,139);
        System.out.println("Weight is: "+ tomato.getWeight());
        System.out.println("Ccal is:" + tomato1.ccal);

    }
}
